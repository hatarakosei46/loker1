<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lowongan Kerja Web</title>
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/uikit.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">

</head>

<body>
    <header>
        <nav class="uk-navbar uk-navbar-container navbar uk-margin">
            <div class="uk-navbar-left">
                <a href="javascript:void();">
                    <h4> <strong>Lowker Webs</strong></h4>
                </a>
            </div>

            <div class="uk-navbar-right">
                <div class="hide__xl uk-hidden@s">
                    <a class="uk-navbar-toggle" type="button" uk-toggle="target: #offcanvas-flip"
                        href="javascript:void();">
                        <span uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">Menu</span>
                    </a>
                </div>

                <ul class="uk-navbar-nav">
                    <li class="uk-active"><a href="#">Beranda</a></li>
                    <li><a href="#">Pembelajaran</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">Tentang Kami</a></li>
                </ul>
            </div>




            <div id="offcanvas-flip" uk-offcanvas="flip: true; overlay: true">
                <div class="uk-offcanvas-bar">

                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                    <ul class="uk-nav uk-nav-default">
                        <br>
                        <li class="uk-active">
                            <a href="javascript:void();">
                                <img class="guest__avatar" src="assets/img/logo-avatar.png" alt="guest-logo">
                                <h3>Guest</h3>
                            </a>
                        </li>
                        <li class="uk-parent">
                            <div class="uk-margin">
                                <form class="uk-search uk-search-default">
                                    <a href="javascript:void();" class="uk-search-icon-flip" uk-search-icon></a>
                                    <input class="uk-search-input" type="search" placeholder="Cari Sesuatu?">
                                </form>
                            </div>
                        </li>
                        <li class="uk-nav-header uk-text-center">Main Menu</li>
                        <li><a href="javascript:void();"><span class="uk-margin-small-right"
                                    uk-icon="icon: table"></span> Beranda</a>
                        </li>
                        <li><a href="javascript:void();"><span class="uk-margin-small-right"
                                    uk-icon="icon: thumbnails"></span>
                                Pembelajaran</a>
                        </li>
                        <li><a href="javascript:void();"><span class="uk-margin-small-right"
                                    uk-icon="icon: thumbnails"></span>
                                Gallery</a>
                        </li>
                        <li><a href="javascript:void();"><span class="uk-margin-small-right"
                                    uk-icon="icon: thumbnails"></span>
                                Tentang Kami</a>
                        </li>
                        <li class="uk-nav-header uk-text-center">Login As</li>
                        <li class="li__special">
                            <a class="uk-button uk-button-default" href="javascript:void();">Perusahaan</a>
                        </li>
                        <li class="li__special">
                            <a class="btn__siswa uk-button uk-button-default" href="javascript:void();">Siswa</a>
                        </li>
                        <li class="uk-nav-divider"></li>
                        <li class="uk-text-center"><small>All Rights Reserved &copy Lowker 2020 | Created by :
                                OneCode.id</small>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </header>

   @yield('content')

    @yield('footer')

</body>

<script src="assets/js/uikit.min.js"></script>
<script src="assets/js/uikit-icons.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
<script src="/node_modules/readmore-js/readmore.min.js"></script>

<script>
    //text limiter 
    $('.textLimit').each(function () {
        var $pTag = $(this).find('p');
        if ($pTag.text().length > 100) {
            var shortText = $pTag.text();
            shortText = shortText.substring(0, 200);
            $pTag.addClass('fullArticle').hide();
            shortText += '<a href="coba.html" class="read-more-link"><br> Login untuk lihat </a>';
            $pTag.append('<a href="coba.html" class="read-less-link"> &lt- baca lebih sedikit</a>');
            $(this).append('<p class="preview">' + shortText + '</p>');
        }
    });

    // $(document).on('click', '.read-more-link', function () {
    //     $(this).parent().hide().prev().show();
    // });

    // $(document).on('click', '.read-less-link', function () {
    //     $(this).parent().hide().next().show();
    // });





</script>

</html>