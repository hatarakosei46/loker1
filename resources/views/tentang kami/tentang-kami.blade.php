 @extends('layout/app')

@section('title','Informasi Siswa')

@section('content')

<main>

    <section>
        <div class="tentang-atas" style="padding: 1rem;">
            <h2><strong>Tentang Kami</strong></h2>
            <p style="width: 20rem;"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  
            </p>
        </div>
    </section>
                    
            <section>
                <div class="tentang-isi uk-text-center">
                    <figure class="uk-card uk-card-hover uk-card-body">
                        <img src="assets/img/tentang-icon1.png" alt="Study Image">
                        <figcaption>
                            <h2><strong>Kelas Online</strong></h2>
                            <p>Konsultasikan informasi studi diluar negeri dengan mudah dan cepat</p>
                        </figcaption>
                    </figure>
                    <figure class="uk-card uk-card-hover uk-card-body">
                        <img src="assets/img/tentang-icon2.png" alt="Perusahaan Image">
                        <figcaption>
                            <h2><strong>Lowongan Kerja</strong></h2>
                            <p>Penjelasan singkat tentang CariKerja.com</p>
                        </figcaption>
                    </figure>
                    <figure class="uk-card uk-card-hover uk-card-body">
                        <img src="assets/img/tentang-icon3.png" alt="Perusahaan Image">
                        <figcaption>
                            <h2><strong>Kualitas Video HD</strong></h2>
                            <p>Penjelasan singkat tentang CariKerja.com</p>
                        </figcaption>
                    </figure>
                </div>        
            </section>

            <section>   

                <div class="card-tentang">
                    <h4 class="uk-text-center"><strong>Lowongan Kerja Terbaru</strong></h4>
                </div>



                  
                <div class="uk-position-relative uk-visible-toggle uk-dark uk-margin-medium-top" tabindex="-1" uk-slider>
                     
                    <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@s uk-child-width-1-4@m ">

                       <li>
                            <div class="uk-card uk-card-default uk-card-body uk-card-info uk-card-hover" >
                                <img src="assets/img/MCD.svg">
                                <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                                <hr style="margin-top:-10px;">
                                <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                                <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                                <p>
                                    <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                    <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                                </p>
                               
                                  

                           </div>
                       </li>

                       <li>
                        <div class="uk-card uk-card-default uk-card-body uk-card-info uk-card-hover" >
                            <img src="assets/img/MCD.svg">
                            <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                            <hr style="margin-top:-10px;">
                            <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                            <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                            <p>
                                <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                            </p>
                           
                              

                       </div>
                   </li>
                   <li>
                    <div class="uk-card uk-card-default uk-card-body uk-card-info uk-card-hover" >
                        <img src="assets/img/MCD.svg">
                        <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                        <hr style="margin-top:-10px;">
                        <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                        <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                        <p>
                            <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                            <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                        </p>
                       
                          

                   </div>
               </li>
               <li>
                <div class="uk-card uk-card-default uk-card-body uk-card-info uk-card-hover" >
                    <img src="assets/img/MCD.svg">
                    <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                    <hr style="margin-top:-10px;">
                    <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                    <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                    <p>
                        <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                        <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                    </p>
                   
                      

               </div>
           </li>
           <li>
            <div class="uk-card uk-card-default uk-card-body uk-card-info uk-card-hover" >
                <img src="assets/img/MCD.svg">
                <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                <hr style="margin-top:-10px;">
                <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                <p>
                    <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                    <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                </p>
               
                  

           </div>
       </li>

                   </ul>

                   <a class="uk-position-center-left uk-position-small " href="#"  uk-slider-item="previous"><i class="fas fa-arrow-circle-left fa-2x" style="color: black;"></i></a>
                <a class="uk-position-center-right uk-position-small " href="#" uk-slider-item="next"><i class="fas fa-arrow-circle-right fa-2x" style="color: black;"></i></a>


           </div>

            </section>


    </main>
    @endsection

    @section('footer')

    <footer>
        <div class="flex-footer">
            <ul class="ul-list">
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
        </div>

        <div class="flex-footer-2">
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>

    @endsection
