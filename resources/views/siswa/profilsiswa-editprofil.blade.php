@extends('layout/app')

@section('title','Informasi Siswa')

@section('content')

<main>
       
        <div class="slider-profile">
            
            <button class="uk-button uk-button-default uk-margin-small-right button-tarik" type="button" uk-toggle="target: #offcanvas-slide"
            >Tarik</button>
            <div id="offcanvas-slide" uk-offcanvas="overlay: true">
                <div class="uk-offcanvas-bar"  style="background: #1e87f0;">
            
                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                    <h2 class="uk-text-bold uk-text-left uk-margin-medium-left" style="color: white;">Dashboard</h2>
                    <img class="uk-align-center" src="assets/img/imgprofilesiswa.svg" alt="">
                    <h3 class="uk-text-bold uk-text-center" style="color: white;">Annisa Kumalasari</h3>
                    <p class="uk-text-center uk-margin-large-bottom" style="color: white;">0812-3456-789</p>

                    <div class="uk-margin-medium-left">
                       <a href="profilsiswa-biodata.html"> <button class="uk-button uk-button-text uk-margin-medium-bottom">
                            <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#1.png" alt="">Information</h4> 
                        </button></a>
                        <br>
                        <a href="profilsiswa-fav.html">
                   <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#2.png" alt="">Favorite</h4></button></a>
                    <a href="profilsiswa-information.html">
                     <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#3.png" alt="">Pasang Tawaran</h4></button></a>
                    <a href="profilsiswa-bantuan.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#4.png" alt="">Bantuan</h4></button></a>

                    <br>
                    <br>
                    <br>
                    
                    <a href="index.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#5.png" alt="">Exit</h4></button></a>
                  

                </div>
            </div>
        </div>
       
       <section>


        <div class="information-siswa">

            <div class="uk-width-expand@m uk-padding-large  uk-margin"> 
                <h1 class=" uk-margin-medium-bottom uk-text-bold ">Edit Profile</h1>
                
                    
                    <form class="form-p">
                        
                        <p>Nama Depan</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Nama Belakang</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Email</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Password</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Konfirmasi Password</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Nomor Telepon</p>
                        <input class="uk-input form-editprofil uk-width-1-1" style="width: 517px" type="text" placeholder="">
                        <p>Alamat Rumah</p>
                        <div class="uk-margin">
                            <textarea class="uk-textarea form-editprofil" rows="5" placeholder=""></textarea>
                        </div>
                        <p>Pilih Negara</p>
                                <select class="uk-select" style="width:194px">
                                        <option>Indonesia</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                </select>
                    
                  
                    
                    </form>
                    <a href="#daftarberhasil" uk-toggle><button class="uk-button uk-border-rounded uk-button-primary uk-align-center uk-margin-large-top">Simpan</button></a>
                
                

            </div>
            
            </div>

        </div>

        <div id="daftarberhasil" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                        <div class="uk-text-center">
                        <img  src="assets/img/checklist.svg" alt="">
                        <h4>Terima Kasih Telah Berpartisipasi,</h4>
                        <h4>Klik dibawah ini untuk melihat postingan Anda</h4>
                        <a href=""><h4>Disini</h4></a>
                    </div>

                    
            </div>
        </div>

      </section>

       
      
    </main>
    @endsection

    @section('footer')
     <footer>
        <div class="flex-footer">
            <ul>
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>
    @endsection