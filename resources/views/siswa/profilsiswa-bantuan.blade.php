@extends('layout/app')

@section('title','Informasi Siswa')

@section('content')



    <main>

       <section>
        <div class="slider-profile">
            
            <button class="uk-button uk-button-default uk-margin-small-right button-tarik" type="button" uk-toggle="target: #offcanvas-slide"
            >Tarik</button>
            <div id="offcanvas-slide" uk-offcanvas="overlay: true">
                <div class="uk-offcanvas-bar"  style="background: #1e87f0;">
            
                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                    <h2 class="uk-text-bold uk-text-left uk-margin-medium-left" style="color: white;">Dashboard</h2>
                    <img class="uk-align-center" src="assets/img/imgprofilesiswa.svg" alt="">
                    <h3 class="uk-text-bold uk-text-center" style="color: white;">Annisa Kumalasari</h3>
                    <p class="uk-text-center uk-margin-large-bottom" style="color: white;">0812-3456-789</p>

                    <div class="uk-margin-medium-left">
                       <a href="profilsiswa-biodata.html"> <button class="uk-button uk-button-text uk-margin-medium-bottom">
                            <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#1.png" alt="">Information</h4> 
                        </button></a>
                        <br>
                        <a href="profilsiswa-fav.html">
                   <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#2.png" alt="">Favorite</h4></button></a>
                    <a href="profilsiswa-information.html">
                     <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#3.png" alt="">Pasang Tawaran</h4></button></a>
                    <a href="profilsiswa-bantuan.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#4.png" alt="">Bantuan</h4></button></a>

                    <br>
                    <br>
                    <br>
                    
                    <a href="index.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#5.png" alt="">Exit</h4></button></a>
                  

                </div>
            </div>
        </div>

            <div class="bantuan-siswa">
                
                <h2><strong>Bantuan<strong></h2>
                    <div class="gambar-bantuan uk-text-center">
                    <img width="100%" height="100%" src="assets/img/imgbantuanfix.jpg" ></div>
                    <h2>Bagaimana Cara Ambil Kelas?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                        a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                    
                        <div class="gambar-bantuan">
                    <img width="100%" height="100%" src="assets/img/imgtestimoni.jpg" ></div>  
                    <h2>Cara Mengisi Testimoni</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                        a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                   
            </div>
            <br>
            <br>
            <br>

       </section>

       
      
    </main>

    @endsection

    @section('footer')

     <footer>
        <div class="flex-footer">
            <ul>
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>
    @endsection