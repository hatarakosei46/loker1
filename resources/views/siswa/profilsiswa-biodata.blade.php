@extends('layout/app')

@section('title','Informasi Siswa')

@section('content')

<main>


        <div class="slider-profile">
            
            <button class="uk-button uk-button-default uk-margin-small-right button-tarik" type="button" uk-toggle="target: #offcanvas-slide"
            >Tarik</button>
            <div id="offcanvas-slide" uk-offcanvas="overlay: true">
                <div class="uk-offcanvas-bar"  style="background: #1e87f0;">
            
                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                    <h2 class="uk-text-bold uk-text-left uk-margin-medium-left" style="color: white;">Dashboard</h2>
                    <img class="uk-align-center" src="assets/img/imgprofilesiswa.svg" alt="">
                    <h3 class="uk-text-bold uk-text-center" style="color: white;">Annisa Kumalasari</h3>
                    <p class="uk-text-center uk-margin-large-bottom" style="color: white;">0812-3456-789</p>

                    <div class="uk-margin-medium-left">
                       <a href="profilsiswa-biodata.html"> <button class="uk-button uk-button-text uk-margin-medium-bottom">
                            <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#1.png" alt="">Information</h4> 
                        </button></a>
                        <br>
                        <a href="profilsiswa-fav.html">
                   <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#2.png" alt="">Favorite</h4></button></a>
                    <a href="profilsiswa-information.html">
                     <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#3.png" alt="">Pasang Tawaran</h4></button></a>
                    <a href="profilsiswa-bantuan.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#4.png" alt="">Bantuan</h4></button></a>

                    <br>
                    <br>
                    <br>
                    
                    <a href="index.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#5.png" alt="">Exit</h4></button></a>
                  

                </div>
            </div>
        </div>
       

        <section>
        <div class="uk-container">
            <div class="uk-text-center uk-align-center uk-margin-large-top">
               <div id="bg-biodata" class=" card-biodata uk-card uk-card-default uk-card-body uk-width-expand@s uk-align-center">
                   
                       <h1>Biodata Siswa</h1>
                       <img src="assets/img/profilbiodata.svg" alt="">
                           <ul class="ul-biodata">
                               <li>Code : <span class="value">005</span></li>
                               <li>Nama Panjang : <span class="value">Annisa Kumalasari</span></li>
                               <li>No Telepon : <span class="value">0821-0001-0987</span></li>
                               <li>Tempat, Tanggal Lahir : <span class="value">Yogyakarta, 21 January 1993</span></li>
                               <li>Jenis Kelamin : <span class="value">Wanita</span></li>
                               <li>Tinggi Badan : <span class="value">168 cm</span></li>
                               <li>Negara : <span class="value">Indonesia</span></li>
                               <li>Status : <span class="value">Belum Menikah</span></li>
                               <li>Pendidikan Saat ini : <span class="value">Sarjana S1</span></li>
                               <li>Bahasa Video : <span class="value">Bahasa Inggris</span></li>
                               <li>Hobi : <span class="value">Membaca , Menulis , Liburan</span></li>
                               <li>Profil : <span class="value">Guru SD</span></li>
                               <li>Agama : <span class="value">Islan</span></li>
                               <li>Visa : <span class="value">0123-4444-2213</span></li>
                               <li>Pekerjaan Yang diinginkan : <span class="value">IT</span></li>
                               <li>Kapan bisa bekerja : <span class="value">2020</span></li>
                               <li>Pernah kerja diluar negeri : <span class="value">Sudah</span></li>
                               <li>Pengalaman Kerja  : <span class="value">IT</span></li>
                               <li>Sertifikat : <span class="value">Hotel</span></li>
                               <li>Tingkat Bicara Bahasa : <span class="value">Rendah</span></li>
                               <li>Tingkat Penulisan Bahasa : <span class="value">Biasa</span></li>
                               <li>Tingkat Bacaan Bahasa : <span class="value">Tinggi</span></li>
                               
                               
                           </ul>   
                       <h3>Profil Video</h3>
       
                       <iframe class="vlog" src="https://www.youtube.com/embed/s536907p0ss?autoplay=0&amp;showinfo=0&amp;rel=0&amp;modestbranding=1&amp;playsinline=1" width="1920" height="1080" frameborder="0" allowfullscreen uk-responsive uk-video="automute: false; autoplay:false;;"></iframe>
                           
                    <div class="button-biodata">
                       <button class="uk-button tombol-rekruitment uk-button-primary"  uk-toggle="target: #modal-example">rekrut</button>
                       <p>atau</p>
                       <a href="#"  class="uk-text-bold">Cetak Profile</a>       
                    </div>     
               </div>
          </div>

          

            <!-- This is the modal -->
            <div id="modal-example" uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <h5 class="uk-modal-title">Silahkan Form dibawah</h5>
                   
                    <div class="uk-margin">
                        <label for="inputteks">Email</label>
                        <input class="uk-input inputteks" type="text" placeholder="Your Email">
                    </div>
                    
                    <div class="uk-margin">
                        <label for="inputdesc">Deskripsi</label>
                        <textarea class="uk-textarea inputdesc" rows="5" placeholder="Berisikan mengapa anda tertarik dengan siswa ini..."></textarea>
                    </div>            
                    <p class="uk-text-center">
                        <button class="uk-button uk-button-primary" type="button" uk-toggle="target: #modal-example">Kirim Pesan</button>
                    </p>
                </div>
            </div>

            <!-- modal terimakasih -->
<!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <p>Terima Kasih! telah memilih kandidat kami
            Silahkan Tunggu 24 jam , dan kami akan segera menghubungi anad.</p>
        <p class="uk-text-right">
            <!-- close itu buttonya routing ke homepage ya din -->
            <button class="uk-button uk-button-primary" type="button">Close</button>
        </p>
    </div>
</div>
            
        </section>

    </main>

    @endsection

    @section('footer')
    <footer>
        <div class="flex-footer">
            <ul class="ul-list">
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
        </div>

        <div class="flex-footer-2">
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>
    @endsection